package com.myApp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;
import com.myApp.dao.TollDao;



@Service
@Transactional
public class TollServiceImpl implements TollService {

	@Autowired
	private TollDao tollDao;

	@Override
	public ResponseDto addToll(RequestDto requestDto) {
		
		return tollDao.addToll(requestDto);
	}

	@Override
	public ResponseDto getAllToll(RequestDto requestDto) {
		
		return tollDao.getAllToll(requestDto);
	}

}
