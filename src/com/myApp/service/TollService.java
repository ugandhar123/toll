package com.myApp.service;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;

public interface TollService {
	ResponseDto addToll(RequestDto requestDto);
	ResponseDto getAllToll(RequestDto requestDto);
	
}
