package com.myApp.service;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;

public interface VehicleService {
	ResponseDto addVehicle(RequestDto requestDto);
	ResponseDto getAllVehicle(RequestDto requestDto);
	
}
