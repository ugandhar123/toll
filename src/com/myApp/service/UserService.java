package com.myApp.service;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;

public interface UserService {
	ResponseDto addUser(RequestDto requestDto);
	ResponseDto getAllUser(RequestDto requestDto);
	
}
