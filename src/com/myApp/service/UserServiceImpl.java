package com.myApp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;
import com.myApp.dao.UserDao;


@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public ResponseDto addUser(RequestDto requestDto) {
		
		return userDao.addUser(requestDto);
	}

	@Override
	public ResponseDto getAllUser(RequestDto requestDto) {
		
		return userDao.getAllUser(requestDto);
	}

	
}
