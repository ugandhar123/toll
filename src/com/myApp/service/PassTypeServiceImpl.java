package com.myApp.service;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;
import com.myApp.dao.PassTypeDao;



@Service
@Transactional
public class PassTypeServiceImpl implements PassTypeService {

	@Autowired
	private PassTypeDao passTypeDao;

	@Override
	public ResponseDto addPassType(RequestDto requestDto) {
		
		return passTypeDao.addPassType(requestDto);
	}

	@Override
	public ResponseDto getAllPassType(RequestDto requestDto) {
		
		return passTypeDao.getAllPassType(requestDto);
	}
	
}
