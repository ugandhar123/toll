package com.myApp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;
import com.myApp.dao.PassDao;



@Service
@Transactional
public class PassServiceImpl implements PassService {

	@Autowired
	private PassDao passDao;

	@Override
	public ResponseDto addPass(RequestDto requestDto) {
		
		return passDao.addPass(requestDto);
	}

	@Override
	public ResponseDto getAllPass(RequestDto requestDto) {
		
		return passDao.getAllPass(requestDto);
	}

	
}
