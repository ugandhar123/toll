package com.myApp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;
import com.myApp.dao.VehicleDao;
import com.myApp.model.PassType;



@Service
@Transactional
public class VehicleServiceImpl implements VehicleService {

	@Autowired
	private VehicleDao vehicleDao;

	@Autowired
	PassTypeService passService;
	
	@Override
	public ResponseDto addVehicle(RequestDto requestDto) {
		
		return vehicleDao.addVehicle(requestDto);
	}

	@Override
	public ResponseDto getAllVehicle(RequestDto requestDto) {
		

		ResponseDto responseDto= vehicleDao.getAllVehicle(requestDto);

		if(responseDto.getListVehicle().size()==0)
		{
			PassType passType=new PassType();
			passType.setVehicleType(requestDto.getVehicle().getVehicleType());
			requestDto.setPassType(passType);
			responseDto=passService.getAllPassType(requestDto);
		}
		responseDto.setStatus("Success");
		
		return responseDto;
	}

	
}
