package com.myApp.service;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;

public interface PassTypeService {
	ResponseDto addPassType(RequestDto requestDto);
	ResponseDto getAllPassType(RequestDto requestDto);
	
}
