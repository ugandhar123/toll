package com.myApp.service;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;

public interface CollectionService {
	ResponseDto addCollection(RequestDto requestDto);
	ResponseDto getAllCollection(RequestDto requestDto);
	
}
