package com.myApp.service;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;

public interface PassService {
	ResponseDto addPass(RequestDto requestDto);
	ResponseDto getAllPass(RequestDto requestDto);
	
}
