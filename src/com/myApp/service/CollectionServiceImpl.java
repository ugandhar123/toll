package com.myApp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;
import com.myApp.dao.CollectionDao;



@Service
@Transactional
public class CollectionServiceImpl implements CollectionService {

	@Autowired
	private CollectionDao collectionDao;

	@Override
	public ResponseDto addCollection(RequestDto requestDto) {
		
		return collectionDao.addCollection(requestDto);
	}

	@Override
	public ResponseDto getAllCollection(RequestDto requestDto) {
		
		return collectionDao.getAllCollection(requestDto);
	}

	
	
}
