package com.myApp.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;
import com.myApp.loggerclient.LogUtils;
import com.myApp.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
/**
 * 
 * @author Vijay Kumar Chowdary
 *
 */
@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping(value = "/user")
public class UserController {
	
	private static final Logger logger = Logger.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/adduser.do", method = RequestMethod.POST, produces = "appliUserion/json", headers = "Accept=appliUserion/json")
	public ResponseEntity<String> adduser(@RequestBody RequestDto requestDto, HttpServletRequest request)	throws DocumentException {

 		LogUtils.logInfo(logger, "UserController info. Start");
 		
 		ResponseDto responseDto = userService.addUser(requestDto);

		if (!responseDto.getStatus().equalsIgnoreCase("success"))			 
			responseDto.setStatus("Error");
		 
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			LogUtils.logError(logger, " JSON Processing error !" + e.getMessage());
		}
		
		ResponseEntity responseEntity = new ResponseEntity(jsonInString, HttpStatus.OK);
		
		LogUtils.logInfo(logger, "User Controller with Add User info. End:::");
		
		return responseEntity;

	}
	@RequestMapping(value = "/getAllUser.do", method = RequestMethod.POST, produces = "appliUserion/json", headers = "Accept=appliUserion/json")
	public ResponseEntity<String> getAllUser(@RequestBody RequestDto requestDto, HttpServletRequest request)	throws DocumentException {

 		LogUtils.logInfo(logger, "UserController info. Start");
 		
 		ResponseDto responseDto = userService.getAllUser(requestDto);

		if (!responseDto.getStatus().equalsIgnoreCase("success"))			 
			responseDto.setStatus("Error");
		 
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			LogUtils.logError(logger, " JSON Processing error !" + e.getMessage());
		}
		
		ResponseEntity responseEntity = new ResponseEntity(jsonInString, HttpStatus.OK);
		
		LogUtils.logInfo(logger, "User Controller with Add User info. End:::");
		
		return responseEntity;

	}
	@RequestMapping(value = "/getUser.do", method = RequestMethod.POST, produces = "appliUserion/json", headers = "Accept=appliUserion/json")
	public ResponseEntity<String> getUser(@RequestBody RequestDto requestDto, HttpServletRequest request)	throws DocumentException {

 		LogUtils.logInfo(logger, "UserController info. Start");
 		
 		ResponseDto responseDto = userService.getAllUser(requestDto);

		if (!responseDto.getStatus().equalsIgnoreCase("success"))			 
			responseDto.setStatus("Error");
		 
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			LogUtils.logError(logger, " JSON Processing error !" + e.getMessage());
		}
		
		ResponseEntity responseEntity = new ResponseEntity(jsonInString, HttpStatus.OK);
		
		LogUtils.logInfo(logger, "User Controller with Add User info. End:::");
		
		return responseEntity;

	}
	

}
