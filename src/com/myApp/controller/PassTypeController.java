package com.myApp.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;
import com.myApp.loggerclient.LogUtils;
import com.myApp.service.PassTypeService;
/**
 * 
 * @author Vijay Kumar Chowdary
 *
 */
@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping(value = "/Pass")
public class PassTypeController {
	
	private static final Logger logger = Logger.getLogger(PassTypeController.class);
	
	@Autowired
	private PassTypeService passTypeService;
	
	@RequestMapping(value = "/addPassType.do", method = RequestMethod.POST, produces = "appliPassTypeion/json", headers = "Accept=application/json")
	public ResponseEntity<String> addPassType(@RequestBody RequestDto requestDto, HttpServletRequest request)	throws DocumentException {

 		LogUtils.logInfo(logger, "PassTypeController info. Start");
 		
 		ResponseDto responseDto = passTypeService.addPassType(requestDto);

		if (!responseDto.getStatus().equalsIgnoreCase("success"))			 
			responseDto.setStatus("Error");
		 
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			LogUtils.logError(logger, " JSON Processing error !" + e.getMessage());
		}
		
		ResponseEntity responseEntity = new ResponseEntity(jsonInString, HttpStatus.OK);
		
		LogUtils.logInfo(logger, "PassType Controller with Add PassType info. End:::");
		
		return responseEntity;

	}
	@RequestMapping(value = "/getAllPassType.do", method = RequestMethod.POST, produces = "appliPassTypeion/json", headers = "Accept=appliPassTypeion/json")
	public ResponseEntity<String> getAllPassType(@RequestBody RequestDto requestDto, HttpServletRequest request)	throws DocumentException {

 		LogUtils.logInfo(logger, "PassTypeController info. Start");
 		
 		ResponseDto responseDto = passTypeService.getAllPassType(requestDto);

		if (!responseDto.getStatus().equalsIgnoreCase("success"))			 
			responseDto.setStatus("Error");
		 
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			LogUtils.logError(logger, " JSON Processing error !" + e.getMessage());
		}
		
		ResponseEntity responseEntity = new ResponseEntity(jsonInString, HttpStatus.OK);
		
		LogUtils.logInfo(logger, "PassType Controller with Add PassType info. End:::");
		
		return responseEntity;

	}
	
}
