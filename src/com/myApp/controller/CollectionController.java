package com.myApp.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;
import com.myApp.loggerclient.LogUtils;
import com.myApp.service.CollectionService;

/**
 * 
 * @author Vijay Kumar Chowdary
 *
 */
@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping(value = "/Collection")
public class CollectionController {
	
	private static final Logger logger = Logger.getLogger(CollectionController.class);
	
	@Autowired
	private CollectionService collection;
	
	@RequestMapping(value = "/addCollection.do", method = RequestMethod.POST, produces = "appliCollectionion/json", headers = "Accept=application/json")
	public ResponseEntity<String> addCollection(@RequestBody RequestDto requestDto, HttpServletRequest request) {

 		LogUtils.logInfo(logger, "CollectionController info. Start");
 		
 		ResponseDto responseDto = collection.addCollection(requestDto);

		if (!responseDto.getStatus().equalsIgnoreCase("success"))			 
			responseDto.setStatus("Error");
		 
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			LogUtils.logError(logger, " JSON Processing error !" + e.getMessage());
		}
		
		ResponseEntity responseEntity = new ResponseEntity(jsonInString, HttpStatus.OK);
		
		LogUtils.logInfo(logger, "Collection Controller with Add Collection info. End:::");
		
		return responseEntity;

	}
	@RequestMapping(value = "/getAllCollection.do", method = RequestMethod.POST, produces = "appliCollectionion/json", headers = "Accept=appliCollectionion/json")
	public ResponseEntity<String> getAllCollection(@RequestBody RequestDto requestDto, HttpServletRequest request) {

 		LogUtils.logInfo(logger, "CollectionController info. Start");
 		
 		ResponseDto responseDto = collection.getAllCollection(requestDto);

		if (!responseDto.getStatus().equalsIgnoreCase("success"))			 
			responseDto.setStatus("Error");
		 
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			LogUtils.logError(logger, " JSON Processing error !" + e.getMessage());
		}
		
		ResponseEntity responseEntity = new ResponseEntity(jsonInString, HttpStatus.OK);
		
		LogUtils.logInfo(logger, "Collection Controller with Add Collection info. End:::");
		
		return responseEntity;

	}

}
