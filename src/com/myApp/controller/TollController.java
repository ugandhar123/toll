package com.myApp.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;
import com.myApp.loggerclient.LogUtils;
import com.myApp.service.TollService;
import com.fasterxml.jackson.core.JsonProcessingException;
/**
 * 
 * @author Vijay Kumar Chowdary
 *
 */
@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping(value = "/Toll")
public class TollController {
	
	private static final Logger logger = Logger.getLogger(TollController.class);
	
	@Autowired
	private TollService tollService;
	
	@RequestMapping(value = "/addToll.do", method = RequestMethod.POST, produces = "appliTollion/json", headers = "Accept=application/json")
	public ResponseEntity<String> addToll(@RequestBody RequestDto requestDto, HttpServletRequest request)	throws DocumentException {

 		LogUtils.logInfo(logger, "TollController info. Start");
 		
 		ResponseDto responseDto = tollService.addToll(requestDto);

		if (!responseDto.getStatus().equalsIgnoreCase("success"))			 
			responseDto.setStatus("Error");
		 
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			LogUtils.logError(logger, " JSON Processing error !" + e.getMessage());
		}
		
		ResponseEntity responseEntity = new ResponseEntity(jsonInString, HttpStatus.OK);
		
		LogUtils.logInfo(logger, "Toll Controller with Add Toll info. End:::");
		
		return responseEntity;

	}
	@RequestMapping(value = "/getAllToll.do", method = RequestMethod.POST, produces = "appliTollion/json", headers = "Accept=appliTollion/json")
	public ResponseEntity<String> getAllToll(@RequestBody RequestDto requestDto, HttpServletRequest request)	throws DocumentException {

 		LogUtils.logInfo(logger, "TollController info. Start");
 		
 		ResponseDto responseDto = tollService.getAllToll(requestDto);

		if (!responseDto.getStatus().equalsIgnoreCase("success"))			 
			responseDto.setStatus("Error");
		 
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			LogUtils.logError(logger, " JSON Processing error !" + e.getMessage());
		}
		
		ResponseEntity responseEntity = new ResponseEntity(jsonInString, HttpStatus.OK);
		
		LogUtils.logInfo(logger, "Toll Controller with Add Toll info. End:::");
		
		return responseEntity;

	}
	
}
