package com.myApp.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Booth")
public class Booth implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name = "boothID", length = 10)
	private int boothID;
	
	// toll foriegn key
	@ManyToOne
	@JoinColumn(name ="tollID")
	private Toll toll;
	
	@Column(name = "boothDirection", length = 20)
	private String boothDirection;
	
	@Column(name = "boothAddress", length = 30)
	private String boothAddress;
	
	@Column(name = "contactNo", length = 15)
	private int contactNo;
	
	public int getBoothID() {
		return boothID;
	}

	public void setBoothID(int boothID) {
		this.boothID = boothID;
	}

	public Toll getToll() {
		return toll;
	}

	public void setToll(Toll toll) {
		this.toll = toll;
	}

	public String getBoothDirection() {
		return boothDirection;
	}

	public void setBoothDirection(String boothDirection) {
		this.boothDirection = boothDirection;
	}

	public String getBoothAddress() {
		return boothAddress;
	}

	public void setBoothAddress(String boothAddress) {
		this.boothAddress = boothAddress;
	}

	public int getContactNo() {
		return contactNo;
	}

	public void setContactNo(int contactNo) {
		this.contactNo = contactNo;
	}

	
}
