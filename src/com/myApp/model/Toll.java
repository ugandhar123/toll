package com.myApp.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Toll")
public class Toll implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name = "tollID", length = 10)
	private int tollID;
	
	@Column(name = "tollDirection", length = 20)
	private String tollDirection;
	
	@Column(name = "tollAddress", length = 30)
	private String tollAddress;
	
	@Column(name = "contactNo", length = 15)
	private String contactNo;
	
		public int getTollID() {
		return tollID;
	}

	public void setTollID(int tollID) {
		this.tollID = tollID;
	}

	public String getTollDirection() {
		return tollDirection;
	}

	public void setTollDirection(String tollDirection) {
		this.tollDirection = tollDirection;
	}

	public String getTollAddress() {
		return tollAddress;
	}

	public void setTollAddress(String tollAddress) {
		this.tollAddress = tollAddress;
	}

	

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
					
}
