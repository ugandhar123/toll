package com.myApp.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PassType")
public class PassType implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name = "PassTypeID", length = 10)
	private int passTypeID;
	
	@Column(name = "passType", length = 20)
	private String passType;
	
	@Column(name = "passCost", length = 5)
	private int passCost;
	
	@Column(name = "passPeriod", length = 10)
	private String passPeriod;
	
	@Column(name = "vehicleType", length = 10)
	private String vehicleType;
	
	@Column(name = "boothID", length = 20)
	private int boothID;
	
	
	
	
	public int getBoothID() {
		return boothID;
	}

	public void setBoothID(int boothID) {
		this.boothID = boothID;
	}

	public int getPassTypeID() {
		return passTypeID;
	}

	public void setPassTypeID(int passTypeID) {
		this.passTypeID = passTypeID;
	}

	public String getPassType() {
		return passType;
	}

	public void setPassType(String passType) {
		this.passType = passType;
	}

	public int getPassCost() {
		return passCost;
	}

	public void setPassCost(int passCost) {
		this.passCost = passCost;
	}

	public String getPassPeriod() {
		return passPeriod;
	}

	public void setPassPeriod(String passPeriod) {
		this.passPeriod = passPeriod;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	
					
}
