package com.myApp.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "User")
public class User implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name = "UserID", length = 256)
	private int UserID;
	
	@Column(name = "User_catID")
	private int User_catID;
	@Column(name = "UserName", length = 256)
	private String UserName;
	
	@Column(name = "dateOfBirth", length = 256)
	private String dateOfBirth;
	
	@Column(name = "gender", length = 256)
	private String gender;
	
	@Column(name = "phoneNumber", length = 12)
	private String phoneNumber;
	
	@OneToOne(targetEntity=Role.class,cascade=CascadeType.ALL) 
	private Role role;

	@Override
	public String toString() {
		return "User [UserID=" + UserID + ", User_catID=" + User_catID + ", UserName=" + UserName + ", dateOfBirth="
				+ dateOfBirth + ", gender=" + gender + ", phoneNumber=" + phoneNumber + ", role=" + role + "]";
	}

	public int getUserID() {
		return UserID;
	}

	public void setUserID(int userID) {
		UserID = userID;
	}

	public int getUser_catID() {
		return User_catID;
	}

	public void setUser_catID(int user_catID) {
		User_catID = user_catID;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	} 

				
}
