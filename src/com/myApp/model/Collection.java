package com.myApp.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Collection")
public class Collection implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name = "collectionID", length = 10)
	private int collectionID;
	
	@Column(name = "boothNo", length = 3)
	private int boothNo;
	
	@Column(name = "newOrOld", length = 3)
	private String newOrOld;
	
	@Column(name = "collectionAmount", length = 20)
	private int collectionAmount;

	public int getCollectionID() {
		return collectionID;
	}

	public void setCollectionID(int collectionID) {
		this.collectionID = collectionID;
	}

	
	public int getBoothNo() {
		return boothNo;
	}

	public void setBoothNo(int boothNo) {
		this.boothNo = boothNo;
	}

	public String getNewOrOld() {
		return newOrOld;
	}

	public void setNewOrOld(String newOrOld) {
		this.newOrOld = newOrOld;
	}

	public int getCollectionAmount() {
		return collectionAmount;
	}

	public void setCollectionAmount(int collectionAmount) {
		this.collectionAmount = collectionAmount;
	}

		
	
}
