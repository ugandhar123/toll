package com.myApp.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Vehicle")
public class Vehicle implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name = "vehicleID", length = 10)
	private int vehicleID;
	
	@Column(name = "vehicleType", length = 20)
	private String vehicleType;
	
	@Column(name = "vehicleRegNo", length = 10)
	private String vehicleRegNo;

	@Column(name = "passTakenDate", length = 3)
	private Date passTakenDate;
	
	@OneToOne
	@JoinColumn(name = "passID", nullable=false)
	private Pass passID;
	
	

	public int getVehicleID() {
		return vehicleID;
	}

	public void setVehicleID(int vehicleID) {
		this.vehicleID = vehicleID;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	
	public String getVehicleRegNo() {
		return vehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}

	public Pass getPassID() {
		return passID;
	}

	public void setPassID(Pass passID) {
		this.passID = passID;
	}

	public Date getPassTakenDate() {
		return passTakenDate;
	}

	public void setPassTakenDate(Date passTakenDate) {
		this.passTakenDate = passTakenDate;
	}
	
	
					
}
