package com.myApp.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Role")
public class Role implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name = "roleID", length = 256)
	private int roleID;
	
	@OneToOne(targetEntity=User.class)
	private User user;  

	
	@Column(name = "roleName", length = 50)
	private String roleName;
	
	@Column(name = "roleCode", length = 10)
	private String roleCode;

	
	@Override
	public String toString() {
		return "Role [roleID=" + roleID + ", user=" + user + ", roleName=" + roleName + ", roleCode=" + roleCode + "]";
	}

	public int getRoleID() {
		return roleID;
	}

	public void setRoleID(int roleID) {
		this.roleID = roleID;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	} 
	
	
}
