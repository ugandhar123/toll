package com.myApp.dao;

import java.util.Arrays;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;
import com.myApp.model.Collection;

//@Transactional
@Repository
public class CollectionDaoImpl implements CollectionDao{

	@Autowired
	HibernateTemplate hibernateTemplate;

	@Override
	public ResponseDto addCollection(RequestDto requestDto) {
		ResponseDto responseDto=new ResponseDto();
		try
		{
			hibernateTemplate.save(requestDto.getCollection());
			
			responseDto.setStatus("Success");
			
			return responseDto;
		}
		catch(Exception e)
		{
			responseDto.setStatus("Error");
			return responseDto;
		}
	}

	@Override
	public ResponseDto getAllCollection(RequestDto requestDto) {
		ResponseDto responseDto=new ResponseDto();
		try
		{
			Query query=hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("select SUM(collectionAmount) from Collection where boothNo=:boothid");
			query.setParameter("boothid", (int)requestDto.getCollection().getBoothNo());
			Collection collection=new Collection();
			//collection.setBoothID(requestDto.getCollection().getBoothID());
			collection.setCollectionAmount(Integer.parseInt(query.list().get(0).toString()));
			responseDto.setListCollection(Arrays.asList(collection));
			responseDto.setStatus("Success");
		}
		catch(Exception e)
		{
			responseDto.setStatus("Error");
		}
		return responseDto;
	}

}


