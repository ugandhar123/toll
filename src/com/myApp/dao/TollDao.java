package com.myApp.dao;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;

public interface TollDao {

	public ResponseDto addToll(RequestDto requestDto);
	public ResponseDto getAllToll(RequestDto requestDto);

}
