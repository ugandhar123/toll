package com.myApp.dao;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.myApp.common.DateDiffer;
import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;
import com.myApp.model.Collection;
import com.myApp.model.Pass;
import com.myApp.model.PassType;
import com.myApp.service.CollectionService;
import com.myApp.service.VehicleService;

//@Transactional
@Repository
public class PassDaoImpl implements PassDao{

	@Autowired
	HibernateTemplate hibernateTemplate;
	
	@Autowired
	VehicleService vehicleService;
	
	@Autowired
	CollectionService collectionService;
	
	@Override
	public ResponseDto addPass(RequestDto requestDto) {
		ResponseDto responseDto=new ResponseDto();
		try
		{
			PassType passType=(PassType)hibernateTemplate.get(PassType.class, requestDto.getPass().getPassType().getPassTypeID());
			Pass pass=requestDto.getPass();
			pass.setPassType(passType);
			pass.setIsExpired(1);//active
			
			String passPeriod=passType.getPassPeriod();
			if(passPeriod.equals("one day"))
			{
				Date date= new  Date();
				String dateString=date.getYear()+"-"+date.getMonth()+"-"+date.getDay();
				String expiryDate=new DateDiffer().getDate(dateString, 1);
				pass.setExpiryDate(expiryDate);
			}
			else if(passPeriod.equals("one month")){
				Date date= new  Date();
				String dateString=date.getYear()+"-"+date.getMonth()+"-"+date.getDay();
				String expiryDate=new DateDiffer().getDate(dateString, 30);
				pass.setExpiryDate(expiryDate);
			}
			
			hibernateTemplate.save(pass);
			//Create vehicle table
			requestDto.setPass(pass);
			responseDto=vehicleService.addVehicle(requestDto);
			
			//Create collection table record
			Collection collection=new Collection();
			collection.setCollectionAmount(passType.getPassCost());
			collection.setBoothNo(passType.getBoothID());
			
			collection.setNewOrOld("new");
			requestDto.setCollection(collection);
			collectionService.addCollection(requestDto);
			//end creation table record
			
			responseDto.setStatus("Success");
			
			return responseDto;
		}
		catch(Exception e)
		{
			responseDto.setStatus("Error");
			return responseDto;
		}
	}

	@Override
	public ResponseDto getAllPass(RequestDto requestDto) {
		ResponseDto responseDto=new ResponseDto();
		try
		{
			List<Pass> listPass= hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("from Pass where PassID=:PassID").list();
			
			responseDto.setListPass(listPass);
			responseDto.setStatus("Success");
		}
		catch(Exception e)
		{
			responseDto.setStatus("Error");
		}
		return responseDto;
	}

}


