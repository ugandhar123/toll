package com.myApp.dao;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;

public interface PassTypeDao {

	public ResponseDto addPassType(RequestDto requestDto);
	public ResponseDto getAllPassType(RequestDto requestDto);
}
