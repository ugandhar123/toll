package com.myApp.dao;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;

public interface VehicleDao {

	public ResponseDto addVehicle(RequestDto requestDto);
	public ResponseDto getAllVehicle(RequestDto requestDto);
}
