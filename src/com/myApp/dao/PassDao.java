package com.myApp.dao;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;

public interface PassDao {

	public ResponseDto addPass(RequestDto requestDto);
	public ResponseDto getAllPass(RequestDto requestDto);

}
