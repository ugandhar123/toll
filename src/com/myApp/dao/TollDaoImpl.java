package com.myApp.dao;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;
import com.myApp.model.Booth;
import com.myApp.model.Toll;

//@Transactional
@Repository
public class TollDaoImpl implements TollDao{

	@Autowired
	HibernateTemplate hibernateTemplate;
	
	@Override
	public ResponseDto addToll(RequestDto requestDto) {
		ResponseDto responseDto=new ResponseDto();
		try
		{
			/*Toll Toll=requestDto.getToll();
			hibernateTemplate.save(Toll);*/
			Toll toll=requestDto.getToll();
			hibernateTemplate.save(toll);
			
			List<Booth> listBooth=requestDto.getListBooth();
			Iterator<Booth> ite=listBooth.iterator();
			while(ite.hasNext())
			{
				Booth booth=(Booth)ite.next();
				booth.setToll(toll);
				hibernateTemplate.save(booth);
			}
			
			responseDto.setStatus("Success");
			
			return responseDto;
		}
		catch(Exception e)
		{
			responseDto.setStatus("Error");
			return responseDto;
		}
	}

	@Override
	public ResponseDto getAllToll(RequestDto requestDto) {
		ResponseDto responseDto=new ResponseDto();
		try
		{
			List<Toll> listToll= hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("from Toll where TollID=:TollID").list();
			
			responseDto.setListToll(listToll);
			responseDto.setStatus("Success");
		}
		catch(Exception e)
		{
			responseDto.setStatus("Error");
		}
		return responseDto;
	}

}


