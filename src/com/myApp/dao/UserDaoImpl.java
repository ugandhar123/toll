package com.myApp.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;
import com.myApp.model.User;

//@Transactional
@Repository
public class UserDaoImpl implements UserDao{

	@Autowired
	HibernateTemplate hibernateTemplate;
	
	
	@Override
	public ResponseDto addUser(RequestDto requestDto) {
		ResponseDto responseDto=new ResponseDto();
		try
		{
			User User=requestDto.getUser();
			hibernateTemplate.save(User);
			responseDto.setStatus("Success");
			return responseDto;
		}
		catch(Exception e)
		{
			responseDto.setStatus("Error");
			return responseDto;
		}
	}

	@Override
	public ResponseDto getAllUser(RequestDto requestDto) {
		ResponseDto responseDto=new ResponseDto();
		try
		{
			List<User> listUser= hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("from User where userID=:userID").list();
			
			responseDto.setListUser(listUser);
			responseDto.setStatus("Success");
		}
		catch(Exception e)
		{
			responseDto.setStatus("Error");
		}
		return responseDto;
	}
}


