package com.myApp.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;
import com.myApp.model.PassType;
import com.myApp.model.Vehicle;
import com.myApp.service.PassTypeService;

//@Transactional
@Repository
public class VehicleDaoImpl implements VehicleDao{

	@Autowired
	HibernateTemplate hibernateTemplate;
	
	@Autowired
	PassTypeService passService;
	
	@Override
	public ResponseDto addVehicle(RequestDto requestDto) {
		ResponseDto responseDto=new ResponseDto();
		try
		{
			//Pass pass=(Pass)hibernateTemplate.get(Pass.class, requestDto.getPass().getPassID());
			
			if(requestDto==null) throw new Exception(); 
			Vehicle vehicle=requestDto.getVehicle();
			vehicle.setPassTakenDate(new Date());
			vehicle.setPassID(requestDto.getPass());
			hibernateTemplate.save(vehicle);
			responseDto.setStatus("Success");
			
			//Update passTable
			
			//end pass table
			return responseDto;
		}
		catch(Exception e)
		{
			responseDto.setStatus("Error");
			return responseDto;
		}
	}

	@Override
	public ResponseDto getAllVehicle(RequestDto requestDto) {
		ResponseDto responseDto=new ResponseDto();
		try
		{
			Query query=hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("from Vehicle where vehicleRegNo=:vehicleRegNo");
			query.setParameter("vehicleRegNo", requestDto.getVehicle().getVehicleRegNo());
			
			List<Vehicle> listVehicle= query.list();
			responseDto.setListVehicle(listVehicle);
			
		}
		catch(Exception e)
		{
			responseDto.setStatus("Error");
		}
		return responseDto;
	}
}


