package com.myApp.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;
import com.myApp.model.Pass;

//@Transactional
@Repository
public class PassTypeDaoImpl implements PassTypeDao{

	@Autowired
	HibernateTemplate hibernateTemplate;
	
	@Override
	public ResponseDto addPassType(RequestDto requestDto) {
		ResponseDto responseDto=new ResponseDto();
		try
		{
			hibernateTemplate.save(requestDto.getPassType());
			responseDto.setStatus("Success");
			
			return responseDto;
		}
		catch(Exception e)
		{
			responseDto.setStatus("Error");
			return responseDto;
		}
	} 

	@Override
	public ResponseDto getAllPassType(RequestDto requestDto) {
		ResponseDto responseDto=new ResponseDto();
		try
		{
			if(requestDto.getPassType().getVehicleType()==null || requestDto.getPassType().getVehicleType().equalsIgnoreCase(""))
			{
				Query query=hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("from PassType where passTypeID=:passTypeID");
				query.setParameter("passTypeID", requestDto.getPassType().getPassTypeID());
				List<Pass> listPass= query.list();
				responseDto.setListPass(listPass);
				
			}
			else
			{
				Query query=hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("from PassType where vehicleType=:vehicleType");
				query.setParameter("vehicleType", requestDto.getPassType().getVehicleType());
				List<Pass> listPass= query.list();
				responseDto.setListPass(listPass);
			
			}
			responseDto.setStatus("Success");
		}
		catch(Exception e)
		{
			responseDto.setStatus("Error");
		}
		return responseDto;
	}

	
}


