package com.myApp.dao;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;

public interface CollectionDao {

	public ResponseDto addCollection(RequestDto requestDto);
	public ResponseDto getAllCollection(RequestDto requestDto);

}
