package com.myApp.dao;

import com.myApp.common.RequestDto;
import com.myApp.common.ResponseDto;

public interface UserDao {

	public ResponseDto addUser(RequestDto requestDto);
	public ResponseDto getAllUser(RequestDto requestDto);

}
