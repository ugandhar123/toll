package com.myApp.common;

import java.util.List;

import com.myApp.model.Booth;
import com.myApp.model.Collection;
import com.myApp.model.Pass;
import com.myApp.model.PassType;
import com.myApp.model.Role;
import com.myApp.model.Toll;
import com.myApp.model.User;
import com.myApp.model.Vehicle;

public class RequestDto {
	private User  user;
	private Role role;
	private Toll toll;
	private Pass pass;
	private Vehicle vehicle;
	private PassType passType;
	private Collection collection;
	private List<Booth> listBooth;
	private String query;
	
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public Toll getToll() {
		return toll;
	}
	public void setToll(Toll toll) {
		this.toll = toll;
	}
	public Pass getPass() {
		return pass;
	}
	public void setPass(Pass pass) {
		this.pass = pass;
	}
	public Vehicle getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	public PassType getPassType() {
		return passType;
	}
	public void setPassType(PassType passType) {
		this.passType = passType;
	}
	public Collection getCollection() {
		return collection;
	}
	public void setCollection(Collection collection) {
		this.collection = collection;
	}
	public List<Booth> getListBooth() {
		return listBooth;
	}
	public void setListBooth(List<Booth> listBooth) {
		this.listBooth = listBooth;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	
	
	
}
