package com.myApp.common;

import java.util.List;

import com.myApp.model.Collection;
import com.myApp.model.Pass;
import com.myApp.model.PassType;
import com.myApp.model.Role;
import com.myApp.model.Toll;
import com.myApp.model.User;
import com.myApp.model.Vehicle;

public class ResponseDto {
	private String status;
	private User User;
	private Role product;
	private List<User> listUser;
	private List<Toll> listToll;
	private List<Pass> listPass;
	private List<Vehicle> listVehicle;
	private List<PassType> listpassType;
	private List<Collection> listCollection;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public User getUser() {
		return User;
	}
	public void setUser(User user) {
		User = user;
	}
	public Role getProduct() {
		return product;
	}
	public void setProduct(Role product) {
		this.product = product;
	}
	public List<User> getListUser() {
		return listUser;
	}
	public void setListUser(List<User> listUser) {
		this.listUser = listUser;
	}
	public List<Toll> getListToll() {
		return listToll;
	}
	public void setListToll(List<Toll> listToll) {
		this.listToll = listToll;
	}
	public List<Pass> getListPass() {
		return listPass;
	}
	public void setListPass(List<Pass> listPass) {
		this.listPass = listPass;
	}
	public List<Vehicle> getListVehicle() {
		return listVehicle;
	}
	public void setListVehicle(List<Vehicle> listVehicle) {
		this.listVehicle = listVehicle;
	}
	public List<PassType> getListpassType() {
		return listpassType;
	}
	public void setListpassType(List<PassType> listpassType) {
		this.listpassType = listpassType;
	}
	public List<Collection> getListCollection() {
		return listCollection;
	}
	public void setListCollection(List<Collection> listCollection) {
		this.listCollection = listCollection;
	}
	
	
	
}
