-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.22


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema toll_booth
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ toll_booth;
USE toll_booth;

--
-- Table structure for table `toll_booth`.`booth`
--

DROP TABLE IF EXISTS `booth`;
CREATE TABLE `booth` (
  `boothID` int(11) NOT NULL AUTO_INCREMENT,
  `boothAddress` varchar(30) DEFAULT NULL,
  `boothDirection` varchar(20) DEFAULT NULL,
  `contactNo` int(11) DEFAULT NULL,
  `tollID` int(11) DEFAULT NULL,
  PRIMARY KEY (`boothID`),
  KEY `FK_8rmq3oqmqlbwvut982ystwr2a` (`tollID`),
  CONSTRAINT `FK_8rmq3oqmqlbwvut982ystwr2a` FOREIGN KEY (`tollID`) REFERENCES `toll` (`tollID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toll_booth`.`booth`
--

/*!40000 ALTER TABLE `booth` DISABLE KEYS */;
INSERT INTO `booth` (`boothID`,`boothAddress`,`boothDirection`,`contactNo`,`tollID`) VALUES 
 (7,'Banglore',NULL,0,4),
 (8,'Banglore123',NULL,0,4),
 (9,'Banglore',NULL,0,5),
 (10,'Banglore123',NULL,0,5),
 (11,'Banglore',NULL,0,6),
 (12,'Banglore123',NULL,0,6),
 (13,'Banglore',NULL,0,7),
 (14,'Banglore123',NULL,0,7),
 (15,'Banglore',NULL,0,8),
 (16,'Banglore123',NULL,0,8);
/*!40000 ALTER TABLE `booth` ENABLE KEYS */;


--
-- Table structure for table `toll_booth`.`collection`
--

DROP TABLE IF EXISTS `collection`;
CREATE TABLE `collection` (
  `collectionID` int(11) NOT NULL AUTO_INCREMENT,
  `boothNo` int(11) DEFAULT NULL,
  `collectionAmount` int(11) DEFAULT NULL,
  `newOrOld` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`collectionID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toll_booth`.`collection`
--

/*!40000 ALTER TABLE `collection` DISABLE KEYS */;
INSERT INTO `collection` (`collectionID`,`boothNo`,`collectionAmount`,`newOrOld`) VALUES 
 (1,1,100,'new'),
 (2,1,100,'new');
/*!40000 ALTER TABLE `collection` ENABLE KEYS */;


--
-- Table structure for table `toll_booth`.`pass`
--

DROP TABLE IF EXISTS `pass`;
CREATE TABLE `pass` (
  `passID` int(11) NOT NULL AUTO_INCREMENT,
  `expiryDate` varchar(20) DEFAULT NULL,
  `isExpired` int(11) DEFAULT NULL,
  `PassTypeID` int(11) DEFAULT NULL,
  PRIMARY KEY (`passID`),
  KEY `FK_myi0uo8w5mjyn5icbgn8f0yfb` (`PassTypeID`),
  CONSTRAINT `FK_myi0uo8w5mjyn5icbgn8f0yfb` FOREIGN KEY (`PassTypeID`) REFERENCES `passtype` (`PassTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toll_booth`.`pass`
--

/*!40000 ALTER TABLE `pass` DISABLE KEYS */;
INSERT INTO `pass` (`passID`,`expiryDate`,`isExpired`,`PassTypeID`) VALUES 
 (1,'2020-10-04',1,1),
 (2,'2020-10-04',1,1);
/*!40000 ALTER TABLE `pass` ENABLE KEYS */;


--
-- Table structure for table `toll_booth`.`passtype`
--

DROP TABLE IF EXISTS `passtype`;
CREATE TABLE `passtype` (
  `PassTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `boothID` int(11) DEFAULT NULL,
  `passCost` int(11) DEFAULT NULL,
  `passPeriod` varchar(10) DEFAULT NULL,
  `passType` varchar(20) DEFAULT NULL,
  `vehicleType` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`PassTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toll_booth`.`passtype`
--

/*!40000 ALTER TABLE `passtype` DISABLE KEYS */;
INSERT INTO `passtype` (`PassTypeID`,`boothID`,`passCost`,`passPeriod`,`passType`,`vehicleType`) VALUES 
 (1,1,100,'one day','Daily','2 wheeler');
/*!40000 ALTER TABLE `passtype` ENABLE KEYS */;


--
-- Table structure for table `toll_booth`.`role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `roleID` int(11) NOT NULL AUTO_INCREMENT,
  `roleCode` varchar(10) DEFAULT NULL,
  `roleName` varchar(50) DEFAULT NULL,
  `user_UserID` int(11) DEFAULT NULL,
  PRIMARY KEY (`roleID`),
  KEY `FK_tasgpcqsnpqsmspxxuqchousr` (`user_UserID`),
  CONSTRAINT `FK_tasgpcqsnpqsmspxxuqchousr` FOREIGN KEY (`user_UserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toll_booth`.`role`
--

/*!40000 ALTER TABLE `role` DISABLE KEYS */;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;


--
-- Table structure for table `toll_booth`.`toll`
--

DROP TABLE IF EXISTS `toll`;
CREATE TABLE `toll` (
  `tollID` int(11) NOT NULL AUTO_INCREMENT,
  `contactNo` varchar(15) DEFAULT NULL,
  `tollAddress` varchar(30) DEFAULT NULL,
  `tollDirection` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`tollID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toll_booth`.`toll`
--

/*!40000 ALTER TABLE `toll` DISABLE KEYS */;
INSERT INTO `toll` (`tollID`,`contactNo`,`tollAddress`,`tollDirection`) VALUES 
 (4,'10188000','Banglore',NULL),
 (5,'10188000','Banglore',NULL),
 (6,'10188000','Banglore',NULL),
 (7,'10188000','Banglore',NULL),
 (8,'10188000','Banglore',NULL);
/*!40000 ALTER TABLE `toll` ENABLE KEYS */;


--
-- Table structure for table `toll_booth`.`user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` longtext,
  `User_catID` int(11) DEFAULT NULL,
  `dateOfBirth` longtext,
  `gender` longtext,
  `phoneNumber` varchar(12) DEFAULT NULL,
  `role_roleID` int(11) DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  KEY `FK_9tlqhuxpp3byha18618mvu9rk` (`role_roleID`),
  CONSTRAINT `FK_9tlqhuxpp3byha18618mvu9rk` FOREIGN KEY (`role_roleID`) REFERENCES `role` (`roleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toll_booth`.`user`
--

/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


--
-- Table structure for table `toll_booth`.`vehicle`
--

DROP TABLE IF EXISTS `vehicle`;
CREATE TABLE `vehicle` (
  `vehicleID` int(11) NOT NULL AUTO_INCREMENT,
  `passTakenDate` datetime DEFAULT NULL,
  `vehicleRegNo` varchar(10) DEFAULT NULL,
  `vehicleType` varchar(20) DEFAULT NULL,
  `passID` int(11) NOT NULL,
  PRIMARY KEY (`vehicleID`),
  KEY `FK_iwats7fdokl342q24522ymnr2` (`passID`),
  CONSTRAINT `FK_iwats7fdokl342q24522ymnr2` FOREIGN KEY (`passID`) REFERENCES `pass` (`passID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toll_booth`.`vehicle`
--

/*!40000 ALTER TABLE `vehicle` DISABLE KEYS */;
INSERT INTO `vehicle` (`vehicleID`,`passTakenDate`,`vehicleRegNo`,`vehicleType`,`passID`) VALUES 
 (1,'2020-11-04 10:00:51','TN0059','2 wheeler',1),
 (2,'2020-11-04 10:01:19','TN0065','2 wheeler',2);
/*!40000 ALTER TABLE `vehicle` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
